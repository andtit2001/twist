#pragma once

#include <cstdint>
#include <cstddef>
#include <climits>
#include <atomic>

#include <twist/support/assert.hpp>

// https://en.wikipedia.org/wiki/Futex

namespace twist {
namespace thread {

// Simple wrappers around futex syscall on Linux

int FutexWait(uint32_t* addr, uint32_t expected);

int FutexWake(uint32_t* addr, size_t count);

//////////////////////////////////////////////////////////////////////

class Futex {
 public:
  Futex(uint32_t* addr) : addr_(addr) {
    TWIST_VERIFY(addr_ != nullptr, "Invalid address");
  }

  Futex(std::atomic_uint32_t& state) : Futex((uint32_t*)&state) {
  }

  void Wait(uint32_t expected) {
    FutexWait(addr_, expected);
  }

  void WakeOne() {
    FutexWake(addr_, 1);
  }

  void WakeAll() {
    FutexWake(addr_, INT_MAX);
  }

 private:
  uint32_t* addr_;
};

size_t FutexCallCount();

}  // namespace thread
}  // namespace twist
