#include <twist/thread/stack.hpp>

#include <twist/support/assert.hpp>

#include <cstdint>

#include <pthread.h>
#include <errno.h>

#define CHECK_RESULT(retcode, error) \
  TWIST_VERIFY(retcode == 0, error << ": " << errno)

namespace twist {
namespace thread {

Stack Stack::ThisThread() {
  Stack stack;

  pthread_attr_t attrs;
  int retcode;

#if APPLE
  retcode = pthread_attr_init(&attrs);
  CHECK_RESULT(retcode, "cannot init thread attributes");

  retcode = pthread_attr_getstackaddr(&attrs, (void**)&stack.start_);
  CHECK_RESULT(retcode, "cannot get stack address");

  retcode = pthread_attr_getstacksize(&attrs, &stack.size_);
  CHECK_RESULT(retcode, "cannot get stack size");

#elif LINUX
  retcode = pthread_getattr_np(pthread_self(), &attrs);
  CHECK_RESULT(retcode, "cannot get thread attributes");

  retcode = pthread_attr_getstack(&attrs, (void**)&stack.start_, &stack.size_);
  CHECK_RESULT(retcode, "cannot get stack address and size");

#endif

  retcode = pthread_attr_destroy(&attrs);
  CHECK_RESULT(retcode, "pthread_attr_destroy failed");

  return stack;
}

}  // namespace thread
}  // namespace twist
