#pragma once

#include <twist/test_framework/test.hpp>

#include <twist/support/time.hpp>

////////////////////////////////////////////////////////////////////////////////

class ITestReporter {
 public:
  virtual ~ITestReporter() = default;

  virtual void TestStarted(const ITestPtr& test) = 0;

  virtual void TestFailed(const ITestPtr& test, const std::string& error) = 0;

  virtual void TestPassed(const ITestPtr& test, twist::Duration elapsed) = 0;

  virtual void AllTestsPassed(size_t test_count, twist::Duration elapsed) = 0;
};

using ITestReporterPtr = std::shared_ptr<ITestReporter>;
