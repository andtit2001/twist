#pragma once

#include <twist/support/assert.hpp>

#include <array>
#include <atomic>
#include <functional>

namespace twist {
namespace strand {

//////////////////////////////////////////////////////////////////////

static const size_t kTLSSlots = 1024;

//////////////////////////////////////////////////////////////////////

using TLS = std::array<void*, kTLSSlots>;

//////////////////////////////////////////////////////////////////////

class TLSManager {
 public:
  using Ctor = std::function<void*()>;
  using Dtor = std::function<void(void*)>;

 private:
  struct Slot {
    Ctor ctor;
    Dtor dtor;
  };

 public:
  static TLSManager& Instance() {
    static TLSManager instance;
    return instance;
  }

  size_t AcquireSlot(Ctor ctor, Dtor dtor) {
    size_t index = next_slot_.fetch_add(1);
    TWIST_VERIFY(index < kTLSSlots, "Overcommit");
    slots_[index] = {ctor, dtor};
    return index;
  }

  void* Access(size_t index) {
    return Access(index, GetTLS());
  }

  void* Access(size_t index, TLS& tls) {
    if (tls[index]) {
      return tls[index];
    }
    tls[index] = slots_[index].ctor();
    return tls[index];
  }

  void Destroy(TLS& tls) {
    for (size_t i = 0; i < next_slot_.load(); ++i) {
      if (tls[i]) {
        slots_[i].dtor(tls[i]);
      }
    }
  }

 private:
  static TLS& GetTLS();

 private:
  Slot slots_[kTLSSlots];
  std::atomic<size_t> next_slot_{0};
};

//////////////////////////////////////////////////////////////////////

class ManagedTLS {
 public:
  ManagedTLS() {
    tls_.fill(nullptr);
  }

  TLS& Get() {
    return tls_;
  }

  ~ManagedTLS() {
    TLSManager::Instance().Destroy(tls_);
  }

 private:
  TLS tls_;
};

}  // namespace strand
}  // namespace twist
