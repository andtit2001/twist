#pragma once

#if defined(TWIST_FAULTY)

#include <twist/fault/wrappers/atomic.hpp>

namespace twist {
namespace stdlike {

template <typename T>
using atomic = fault::FaultyAtomic<T>;  // NOLINT

}  // namespace stdlike
}  // namespace twist

#else

#include <atomic>

namespace twist {
namespace stdlike {

using ::std::atomic;

}  // namespace stdlike
}  // namespace twist

#endif
