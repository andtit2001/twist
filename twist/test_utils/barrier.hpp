#pragma once

#include <twist/strand/stdlike.hpp>

namespace twist {
namespace test_utils {

class OnePassBarrier {
 public:
  explicit OnePassBarrier(const size_t num_threads)
      : thread_count_{num_threads} {
  }

  void PassThrough() {
    std::unique_lock<twist::strand::mutex> lock{mutex_};
    --thread_count_;
    if (thread_count_ == 0) {
      all_threads_arrived_.notify_all();
    } else {
      all_threads_arrived_.wait(lock, [this]() { return thread_count_ == 0; });
    }
  }

 private:
  twist::strand::mutex mutex_;
  twist::strand::condition_variable all_threads_arrived_;
  size_t thread_count_;
};

}  // namespace test_utils
}  // namespace twist
