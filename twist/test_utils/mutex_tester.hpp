#pragma once

#include <twist/test_framework/test_framework.hpp>

#include <atomic>

namespace twist {
namespace test_utils {

template <class Mutex>
class MutexTester {
 public:
  template <class... Args>
  MutexTester(Args... args) : mutex_(args...) {
  }

  template <class... Args>
  void Lock(Args... args) {
    mutex_.Lock(args...);
    ASSERT_FALSE_M(already_inside_critical_.exchange(true),
                   "Mutual exclusion violated");
  }

  template <class... Args>
  void Unlock(Args... args) {
    ASSERT_TRUE_M(already_inside_critical_.exchange(false),
                  "Mutual exclusion violated");
    mutex_.Unlock(args...);
  }

 private:
  Mutex mutex_;
  std::atomic<bool> already_inside_critical_{std::memory_order_relaxed};
};

}  // namespace test_utils
}  // namespace twist
