#pragma once

#include <atomic>
#include <functional>

namespace twist {

class XORCheckSum {
 public:
  template <typename T>
  void Feed(const T& item) {
    total_.fetch_xor(std::hash<T>{}(item));
  }

  bool Validate() {
    return total_.load() == 0;
  }

 private:
  std::atomic<size_t> total_{0};
};

}  // namespace twist
