#pragma once

#include <twist/strand/stdlike.hpp>

namespace twist {
namespace fault {

class FaultyMutex {
 public:
  FaultyMutex();

  // std::mutex-like / Lockable
  void lock();      // NOLINT
  bool try_lock();  // NOLINT
  void unlock();    // NOLINT

 private:
  twist::strand::mutex impl_;
  twist::strand::thread_id owner_id_;
};

}  // namespace fault
}  // namespace twist
