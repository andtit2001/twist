#pragma once

#include <twist/context/context.hpp>
#include <twist/context/stack.hpp>

#include <twist/fiber/runtime/api.hpp>

// TODO: ???
#include <twist/strand/tls.hpp>

#include <twist/support/intrusive_list.hpp>

namespace twist {
namespace fiber {

//////////////////////////////////////////////////////////////////////

enum class FiberState { Starting, Runnable, Running, Suspended, Terminated };

class Fiber : public IntrusiveListNode<Fiber> {
 public:
  size_t Id() const {
    return id_;
  }

  context::ExecutionContext& Context() {
    return context_;
  }

  FiberState State() const {
    return state_;
  }

  void SetState(FiberState target) {
    state_ = target;
  }

  strand::TLS& GetFLS() {
    return fls_.Get();
  }

  void InvokeUserRoutine() {
    routine_();
  }

  static Fiber* Create(FiberRoutine routine);
  static void SetupTrampoline(Fiber* fiber);

  ~Fiber();

 private:
  context::Stack stack_;
  context::ExecutionContext context_;
  FiberState state_;
  FiberRoutine routine_;
  strand::ManagedTLS fls_;
  FiberId id_;
};

}  // namespace fiber
}  // namespace twist
