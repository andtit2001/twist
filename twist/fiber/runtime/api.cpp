#include <twist/fiber/runtime/api.hpp>

#include <twist/fiber/runtime/scheduler.hpp>

namespace twist {
namespace fiber {

//////////////////////////////////////////////////////////////////////

void RunScheduler(FiberRoutine main, size_t fuel) {
  Scheduler scheduler;
  scheduler.Run(main, fuel);
}

//////////////////////////////////////////////////////////////////////

void Spawn(FiberRoutine routine) {
  GetCurrentScheduler()->Spawn(routine);
}

void Yield() {
  GetCurrentScheduler()->Yield();
}

void SleepFor(Duration duration) {
  GetCurrentScheduler()->SleepFor(duration);
}

FiberId GetFiberId() {
  return GetCurrentFiber()->Id();
}

}  // namespace fiber
}  // namespace twist
