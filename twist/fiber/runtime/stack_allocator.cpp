#include <twist/fiber/runtime/stack_allocator.hpp>

#include <vector>

namespace twist {
namespace fiber {

using context::Stack;

static size_t BytesToPagesRoundUp(size_t bytes) {
  static const size_t kPageSize = 4096;

  size_t pages = bytes / kPageSize;
  if (bytes % kPageSize != 0) {
    ++pages;
  }
  return pages;
}

class StackAllocator {
  static const size_t kStackSizePages = 8;

 public:
  StackAllocator()
    : size_pages_(kStackSizePages) {
  }

  Stack Allocate() {
    if (!pool_.empty()) {
      Stack stack = std::move(pool_.back());
      pool_.pop_back();
      return stack;
    }
    return Stack::Allocate(size_pages_);
  }

  void Release(Stack stack) {
    pool_.push_back(std::move(stack));
  }

  void SetMinSize(size_t bytes) {
    size_pages_ = BytesToPagesRoundUp(bytes);
  }

 private:
  std::vector<Stack> pool_;
  size_t size_pages_;
};

//////////////////////////////////////////////////////////////////////

static StackAllocator allocator;

Stack AllocateStack() {
  return allocator.Allocate();
}

void ReleaseStack(Stack stack) {
  allocator.Release(std::move(stack));
}

void SetMinStackSize(size_t bytes) {
  allocator.SetMinSize(bytes);
}

}  // namespace fiber
}  // namespace twist
