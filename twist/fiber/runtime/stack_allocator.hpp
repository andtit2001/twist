#pragma once

#include <twist/context/stack.hpp>

namespace twist {
namespace fiber {

context::Stack AllocateStack();
void ReleaseStack(context::Stack stack);

void SetMinStackSize(size_t bytes);

}  // namespace fiber
}  // namespace twist
