#include <twist/support/nullptr.hpp>

#include <twist/support/compiler.hpp>

std::ostream& operator<<(std::ostream& out, std::nullptr_t _) {
  TWIST_UNUSED(_);
  return out << "nullptr";
}
