#include <benchmark/benchmark.h>

#include <twist/fiber/core/api.hpp>

#include <thread>

using namespace twist::fiber;

static void BM_FiberYield(benchmark::State& state) {
  auto main = [&]() {
    for (auto _ : state) {
      Yield();
    }
  };

  RunScheduler(main);
}

BENCHMARK(BM_FiberYield);

static void BM_ThreadYield(benchmark::State& state) {
  for (auto _ : state) {
    std::this_thread::yield();
  }
}

BENCHMARK(BM_ThreadYield);

BENCHMARK_MAIN();

