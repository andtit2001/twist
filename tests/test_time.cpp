#include <twist/support/time.hpp>
#include <twist/support/sleep.hpp>

#include <twist/test_framework/test_framework.hpp>

using twist::Timer;

TEST_SUITE(Time) {
  SIMPLE_TEST(Elapsed) {
    Timer timer;
    twist::SleepMillis(100);
    ASSERT_TRUE(timer.Elapsed() >= std::chrono::milliseconds(100));
  }

  SIMPLE_TEST(Restart) {
    static const auto kPeriod = std::chrono::milliseconds(100);

    Timer timer;

    twist::Sleep(kPeriod);
    auto elapsed = timer.Restart();
    ASSERT_TRUE(elapsed >= kPeriod);

    twist::Sleep(kPeriod);
    elapsed = timer.Elapsed();
    ASSERT_TRUE(elapsed >= kPeriod);
    ASSERT_TRUE(elapsed <= kPeriod * 2);
  }
}
