#include <twist/strand/test.hpp>

#include <twist/strand/stdlike.hpp>
#include <twist/support/locking.hpp>

#include <list>
#include <iostream>

TEST_SUITE(Threading) {

    SIMPLE_T_TEST(Mutex) {
      twist::strand::mutex mutex;
      size_t count = 0;

      static const size_t kIterations = 10;

      auto routine = [&]() {
        for (size_t i = 0; i < kIterations; ++i) {
          twist::strand::this_thread::sleep_for(std::chrono::milliseconds(i * 5));

          auto lock = twist::LockUnique(mutex);

          ASSERT_FALSE(mutex.try_lock());

          std::cout << "thread " << twist::strand::this_thread::get_id()
                    << " in critical section" << std::endl;
          twist::strand::this_thread::yield();
          ++count;
          twist::strand::this_thread::yield();
        }
      };

      static const size_t kThreads = 5;

      std::list<twist::strand::thread> threads;
      for (size_t i = 0; i < kThreads; ++i) {
        threads.emplace_back(routine);
      }
      for (auto& t : threads) {
        t.join();
      }

      ASSERT_EQ(count, kIterations * kThreads);
    }
}
